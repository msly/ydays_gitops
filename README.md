# ydays_gitops

Projet de mise en place d'une chaine d'integration continu d'une application nginx. Et un deploiement continue avec argocd dans un cluster kubernetes avec argocd.

# Les étapes pour tester ce projet
## Prérequis à installer
Docker  
Minikube  
kubectl  
## ouvrir un terminal et lancer ce script
```
    minikube start
    kubectl create namespace argocd
    kubectl create namespace ydays-namespace
    kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/master/manifests/install.yaml
    # noter le mot de passe pour vous connecter à argocd GUI
    kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d
    kubectl port-forward svc/argocd-server -n argocd 8080:443
```  
## connection a argocd UI
username : admin  
password : apparait dans votre terminale grâce à l'avant dernière commande ci-dessus.

## créer l'application argocd
Faire un 
```
    kubectl apply -f application.yml
```

copier le contenu du fichier application.yml dans votre machine.

## accéder à l'application nginx 
http://url-ydays-service:Port  
url-ydays-service correspond à l'IP du minikube obtenu avec la commande 
``` minikube ip ```   
Port: le NodePort du service "ydays-service"
